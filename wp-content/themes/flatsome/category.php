<?php
get_header();
?>

<h1>Product Variant - Original</h1>
<div>
	<img class="categoryImg" src="http://dev.bactershield.co.za/wp-content/uploads/2016/07/Bacter-Shield-South-Africa-Product-Range.jpg">
	<p class="categoryText">It’s such a beautiful thing to see your child having fun unrestrictedly. But while enjoying outside, he also invites germs. At Bacter Shield we understand your concern that’s why new Bacter Shield hand wash is fortified with daily germ guard power to keep your child protected from germs, so that you can enjoy seeing him having only 100% FUN with 0% FEAR.</p>
</div>
<div>
	<div class="catProduct">
		<div class="catImgContainer"><img src="http://dev.bactershield.co.za/wp-content/uploads/2016/07/original-soap-1.png"></div>
		<p>Insert here a description of the original soap something something something</p>
		<a href="http://dev.bactershield.co.za/products/">View All Soaps</a>
	</div>
	<div class="catProduct">
		<div class="catImgContainer"><img src="http://dev.bactershield.co.za/wp-content/uploads/2016/07/BS-SA-Body-Wash-Bottle-Original-1in-1.jpg"></div>
		<p>Insert here a description of the original bodywash something something something</p>
		<a href="http://dev.bactershield.co.za/products/">View All Bodywashes</a>
	</div>
	<div class="catProduct">
		<div class="catImgContainer"><img src="http://dev.bactershield.co.za/wp-content/uploads/2016/07/BS-Original_SA.jpg"></div>
		<p>Insert here a description of the original handwash something something something</p>
		<a href="http://dev.bactershield.co.za/products/">View All Handwashes</a>
	</div>
</div>
<hr class="catPageDivider"></hr>
<div class="row">

<ul>
<?php foreach (get_categories() as $cat) : ?>
<li>
<?php z_taxonomy_image($cat->term_id); ?>
<a href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->cat_name; ?></a>
</li>
<?php endforeach; ?>
</ul>


	<h1 class="otherRangesTitle">Other Ranges</h1>
	<?php 
		$categories = get_categories();
		foreach($categories as $category): 
		if($category->term_id === 1) {
		continue;
		}
	?>

		<div class="otherRange">
			<img src="<?php echo $category->image; ?>"/>
			<h6><?php echo $category->name; ?></h6>
		</div>

	<?php endforeach; ?>
</div>
<?php get_footer(); ?>