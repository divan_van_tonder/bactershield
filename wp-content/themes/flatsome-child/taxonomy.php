<?php
get_header();
?>
<?php $current_category = single_cat_title("", false); ?>

<div class="top-image <?php echo str_replace(' ', '-', strtolower($current_category)); ?>-bg-color"><?php z_taxonomy_image($cat->term_id); ?></div>
<!-- IMAGES & DESCRIPTIONS OF PRODUCTS -->
<div class="row container ">
	<h1 class="cat-title"><?php single_cat_title(''); ?> Range </h1>
	<?php $description = term_description( '', get_query_var( 'taxonomy' ) ); ?>
	<p><?php echo $description; ?></p>
	<div class="row">
		<div class="large-6 columns">Box</div>
		<div class="large-6 columns">Image</div>
	</div>
	<?php 
	$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$args = array(
		'post_type' => 'product',
		'tax_query' => array(
			array(
				'taxonomy' => 'product-category',
				'field' => 'slug',
				'terms' => $current_term->slug
				)
			)
		);
		?>
	<div class="row product-entries">
		<?php $entries = get_posts($args);
		foreach($entries as $entry): 
			$product_image = wp_get_attachment_url(get_post_thumbnail_id($entry->ID));
		?>
		<div class="small-12 large-4 columns">
			<div class="column-inner">
				<div class="ux-img-container">
					<img class="product-image" src="<?php echo $product_image; ?>">
				</div>
				<h1 class="uppercase"><?php echo $entry->post_title; ?></h1>
				<div class="tx-div small clearfix"></div>
				<p><?php echo $entry->post_content; ?></p>
				<a href="/products/">View all ranges</a>
			</div>
		</div>
		<?php endforeach; ?>
	</div>


<!-- OUR VARIANTS SECTION -->
<div class="row container">
	<div class="small-12 large-4 columns">
		<div class="column-inner">
			<div class="tx-div large clearfix"></div><!-- end divider -->
		</div>
	</div>
	<div class="small-12 large-4 columns">
		<div class="column-inner">
			<h2 style="text-align: center;"><span class="uppercase" style="color: #e14d43;">OUR VARIANTS</span></h2>
		</div>
	</div>
	<div class="small-12 large-4 columns">
		<div class="column-inner">
			<div class="tx-div large clearfix"></div><!-- end divider -->
		</div>
	</div>
</div>

<div class="row container custom-width " style="max-width:1200px">
	<div class="small-12 large-2 columns">
		<div class="column-inner">
			<div class="ux-img-container">
				<a href="http://dev.bactershield.co.za/product-category/original" target="_self">
					<img src="http://dev.bactershield.co.za/wp-content/uploads/2016/07/hw-original-1600x1600.png" alt="Original" title="Original" style="bottom:-0px">
				</a>
			</div>
		</div>
		<p style="text-align: center;"><span style="color: #e14d43;">Bacter Shield<br>Original</span></p>
	</div>
	<div class="small-12 large-2 columns">
		<div class="column-inner">	
			<div class="ux-img-container">
				<a href="http://dev.bactershield.co.za/product-category/herbal" target="_self">
					<img src="http://dev.bactershield.co.za/wp-content/uploads/2016/09/herbal-pic.png" alt="Herbal" title="Herbal" style="bottom:-0px">
				</a>
			</div>
			<p style="text-align: center;"><span style="color: #50b848;">Bacter Shield<br>Herbal</span></p>
		</div>
	</div>
	<div class="small-12 large-2 columns">
		<div class="column-inner">
			
			<div class="ux-img-container">
				<a href="http://dev.bactershield.co.za/product-category/spring-fresh" target="_self">
					<img src="http://dev.bactershield.co.za/wp-content/uploads/2016/09/spring-fresh-pic.png" alt="Spring Fresh" title="Spring Fresh" style="bottom:-0px">
				</a>
			</div>
			<p style="text-align: center;"><span style="color: #ffcc00;">Bacter Shield<br>Spring Fresh</span></p>
		</div>
	</div>
	<div class="small-12 large-2 columns">
		<div class="column-inner">
			
			<div class="ux-img-container">
				<a href="http://dev.bactershield.co.za/product-category/gentle-care" target="_self">
					<img src="http://dev.bactershield.co.za/wp-content/uploads/2016/09/gentle-care-pic.png" alt="Gentle Care" title="Gentle Care" style="bottom:-0px">
				</a>
			</div>
			<p style="text-align: center;"><span style="color: #ffa3d9;">Bacter Shield<br>Gentle Care</span></p>
		</div>
	</div>
	<div class="small-12 large-2 columns">
		<div class="column-inner">
			
			<div class="ux-img-container">
				<a href="http://dev.bactershield.co.za/product-category/power-fresh" target="_self">
					<img src="http://dev.bactershield.co.za/wp-content/uploads/2016/09/power-fresh-pic.png" alt="Power Fresh" title="Power Fresh" style="bottom:-0px">
				</a>
			</div>
			<p style="text-align: center;"><span style="color: #00aae7;">Bacter Shield<br>Power Fresh</span></p>
		</div>
	</div>
	<div class="small-12 large-2 columns">
		<div class="column-inner">
			
			<div class="ux-img-container">
				<a href="http://dev.bactershield.co.za/product-category/complete-care" target="_self">
					<img src="http://dev.bactershield.co.za/wp-content/uploads/2016/09/complete-care-pic.png" alt="Complete Care" title="Complete Care" style="bottom:-0px">
				</a>
			</div>
			<p style="text-align: center;"><span style="color: #f2de96;">Bacter Shield<br>Complete Care</span></p>
		</div>
	</div>
</div>
<p class="ourVariantsText">All of our variants are available in various combinations of soap bars, hand washes, and shower gels. To find out more about our product range and variants click on the images above.</p>
<!-- END OF OUR VARIANTS SECTION -->


<?php get_footer(); ?>