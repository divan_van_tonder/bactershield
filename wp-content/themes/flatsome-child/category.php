<?php
get_header();
?>
<?php $current_category = single_cat_title("", false); ?>

<h1 class="productVariantTitle">Product Variant - <?php single_cat_title(''); ?></h1>hghgf
<?php the_category_ID(); ?>
<div class="categoryText">
	<?php
		$categories = get_the_category();
		foreach ( $categories as $category ) { 
   			z_taxonomy_image($cat->term_id);
   			echo category_description( $category_id );
		} 
	?>
</div>

<div>
	<div class="catProduct">
		<div class="catImgContainer"><img src="http://dev.bactershield.co.za/wp-content/uploads/2016/07/original-soap-1.png"></div>
		<p>Insert here a description of the original soap something something something</p>
		<a href="http://dev.bactershield.co.za/products/">View All Soaps</a>
	</div>
	<div class="catProduct">
		<div class="catImgContainer"><img src="http://dev.bactershield.co.za/wp-content/uploads/2016/07/BS-SA-Body-Wash-Bottle-Original-1in-1.jpg"></div>
		<p>Insert here a description of the original bodywash something something something</p>
		<a href="http://dev.bactershield.co.za/products/">View All Bodywashes</a>
	</div>
	<div class="catProduct">
		<div class="catImgContainer"><img src="http://dev.bactershield.co.za/wp-content/uploads/2016/07/BS-Original_SA.jpg"></div>
		<p>Insert here a description of the original handwash something something something</p>
		<a href="http://dev.bactershield.co.za/products/">View All Handwashes</a>
	</div>
</div>

<hr class="catPageDivider"></hr>

<div class="row">
	<h1 class="otherRangesTitle">Other Ranges</h1>
		<?php
		$current_category = get_the_category();
		$current_category_id = 0; 
		if(isset($current_category[0])){
			$current_category_id = $current_category[0]->cat_ID;
		} 
		$args = array( 'exclude' => '1', 'taxonomy' => 'product-category');
		$category_ids = get_categories($args );
		foreach($category_ids as $cat) {
			if($current_category_id === $cat->term_id) {
				continue;
			}
			$link = get_category_link( $cat->term_id ); 
		?>
			<div class="otherRange">
				<a href="<?php echo get_category_link($cat->term_id); ?>">
					<?php z_taxonomy_image($cat->term_id); ?>
					<h6><?php echo $cat->name; ?></h6>
				</a>
			</div>
		<?php
			}
		?>
</div>

<?php get_footer(); ?>